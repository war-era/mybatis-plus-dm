package com.example.dmdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dmdemo.entity.Battle;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author welcome
 */
@Mapper
public interface BattleMapper extends BaseMapper<Battle> {
    @Insert("INSERT INTO test.battle(battle.\"id\",\"nickname\",\"position\") VALUES (#{id},#{nickname},#{position})")
    int insertData(Battle battle);
}
