package com.example.dmdemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author welcome
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "\"TEST\".\"battle\"")
public class Battle {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    @TableField("nickname")
    private String nickname;
    @TableField("position")
    private String position;
}
