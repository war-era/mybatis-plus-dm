package com.example.dmdemo.controller;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
/**
 * @author welcome
 */


@RestController
public class DbController {

    /**
     * 注入 jdbcTemplate 模板对象
     */
    @Resource
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/queryDbVersion")
    public List queryDbVersion() {
        return jdbcTemplate.queryForList(
                "SELECT banner as 版本信息 FROM v$version");
    }
}

