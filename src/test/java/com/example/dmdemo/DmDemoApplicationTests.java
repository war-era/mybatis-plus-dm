package com.example.dmdemo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.dmdemo.entity.Battle;
import com.example.dmdemo.mapper.BattleMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class DmDemoApplicationTests {
    @Resource
    BattleMapper mapper;
    @Test
    void contextLoads() {
        QueryWrapper wrapper=new QueryWrapper<>();

        List list = mapper.selectList(wrapper);
        System.out.println(list);
    }

    @Test
    void test1(){
        Battle battle=new Battle(12,"金蟾","法师");
         mapper.insertData(battle);
        System.out.println();
    }
    @Test
    void test2(){
        Battle battle=new Battle();
        battle.setId(1);
        battle.setNickname("赵怀真");
        battle.setPosition("战士");
        int i = mapper.updateById(battle);
    }
    @Test
    void test3(){
        int i = mapper.deleteById(11);

    }

}
